import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HttpService {

  constructor(private http: HttpClient) { }

  public apipost(url, body, options?){
    return this.http.post(environment.api_base_url+url, body, options);
  }
  public apiget(url){
    return this.http.get(environment.api_base_url+url);
  }
  
}


// function httpService(constants) {
//   var self = this;
//   /**
//    * Query Server
//    * @param  {String}   url      Query URL
//    * @param  {Object}   params   Query params
//    * @param  {Function} callback Callback funtion
//    * @return {[type]}            [description]
//    */

//   self.query = function (url, params, callback) {
//     this.http({
//       method: 'GET',
//       url: (_isurl(url)) ? url : constants.api.path + url,
//       params: params
//     })
//       .then(function (receive) {
//         callback(receive);
//       })
//       .catch(function (reason, cause) {
//         console.error(reason, cause);
//       });
//   }

//   /**
//    * Post Server
//    * @param  {String}   url      Post URL
//    * @param  {Object}   params   Post data
//    * @param  {Function} callback Callback funtion
//    * @return {[type]}            [description]
//    */
//   self.post = function (url, data, callback) {
//     this.http({
//       method: 'POST',
//       url: (_isurl(url)) ? url : constants.api.path + url,
//       transformRequest: function (obj) {
//         var str = [];
//         for (var p in obj)
//           str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
//         return str.join("&");
//       },
//       data: data,
//     })
//       .then(function (receive) {
//         callback(receive);
//       })
//       .catch(function (reason, cause) {
//         console.error(reason, cause);
//       });
//   }

//   /**
//    * Get Server
//    * @param  {String}   url      Get URL
//    * @param  {Function} callback Callback funtion
//    * @return {[type]}            [description]
//    */
//   self.get = function (url, callback) {
//     this.http({
//       method: 'GET',
//       url: constants.api.path + url
//     })
//       .then(function (receive) {
//         callback(receive);
//       })
//       .catch(function (reason, cause) {
//         console.error(reason, cause);
//       });
//   }


//   /**
//    * Mulit-part post Server
//    * @param  {String}   url      Mulit-part post URL
//    * @param  {Object}   params   Mulit-part post data
//    * @param  {Function} callback Callback funtion
//    * @return {[type]}            [description]
//    */
//   self.multi = function (url, data, callback) { }

//   /**
//    * Put Server
//    * @param  {String}   url      Put URL
//    * @param  {Object}   params   Put data
//    * @param  {Function} callback Callback funtion
//    * @return {[type]}            [description]
//    */
//   self.put = function (url, data, callback) {
//     this.http({
//       method: 'PUT',
//       url: (_isurl(url)) ? url : constants.api.path + url,
//       data: data,
//     })
//       .then(function (receive) {
//         callback(receive);
//       })
//       .catch(function (reason, cause) {
//         console.error(reason, cause);
//       });
//   }

//   /**
//    * Delete Server
//    * @param  {String}   url      Delete URL
//    * @param  {Function} callback Callback funtion
//    * @return {[type]}            [description]
//    */
//   self.delete = function (url, data, callback) {
//     this.http({
//       method: 'DELETE',
//       url: (_isurl(url)) ? url : constants.api.path + url,
//       data: data,
//     })
//       .then(function (receive) {
//         callback(receive);
//       })
//       .catch(function (reason, cause) {
//         console.error(reason, cause);
//       });
//   }

//   function _isurl(url) {
//     var url_format = new RegExp(/^(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])$/igm);
//     return url_format.test(url);
//   }
// }

