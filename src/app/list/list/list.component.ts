import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../_services/http/http.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
	public records = new Array();
  constructor(
		private _http: HttpService
	) { }

  ngOnInit() {
	  let send = {	
			'query':'*',
			'params': {
				'wt': 'json',
				'start': 0,
				'rows': 30000,
				'fl': 'store_id, source, version, status, data, unique',
				'sort': 'timestamp desc',
			}
			
		}
		
		this._http.apipost('sPGetAppConfig', send).subscribe(data=>{
			this.records = data['response']['docs'];
			console.log(data);
		});
		
	
		
	// function list(cmsModel){
	// 	var records = new Array();
	// 	var cms_model = new cmsModel();
		
	// 	fetchlistings();

	// 	this.remove = function(item){
	// 		var id = item.unique;
	// 		cms_model.send = {
	// 			'data': {
	// 				'unique': id
	// 			}
	// 		}
	// 		cms_model.remove().then(function(state){
	// 			fetchlistings();
	// 		});
	// 	}

	// 	function fetchlistings(){
	// 		cms_model.send = {
	// 			'query':'*',
	// 			'params': {
	// 				'wt': 'json',
	// 				'start': 0,
	// 				'rows': 30000,
	// 				'fl': 'store_id, source, version, status, data, unique',
	// 				'sort': 'timestamp desc',
	// 			}
	// 		}
	// 		cms_model.search().then(function(state){
	// 			this.records =  cms_model.records;
	// 		});
	// 	}

	// 	this.removeUser = function(data) {
	// 		var deleteUser = window.confirm('Are you absolutely sure you want to delete?');
	// 		if (deleteUser) {
	// 		  window.alert('Going to delete');
	// 		 this.remove(data);
	// 		}
	// 	  }
	// 	}
  // }
  }

}
