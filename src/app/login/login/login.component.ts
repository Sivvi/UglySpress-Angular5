import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertServiceService } from '../../_services/alert-service/alert-service.service';
import { timeout } from 'rxjs/operator/timeout';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  //public message = false;
  constructor(private router: Router, private alertService: AlertServiceService) { }

  ngOnInit() {
  }

  public showloader: boolean = false;

  loginUser(e) {
    e.preventDefault();
    //console.log(e);
    var username = e.target.elements[0].value;
    var password = e.target.elements[1].value;
    var self = this;

    if (username == "admin" && password == "123456") {
      this.router.navigate(['/list']);
    }
    // else (username == "" && password == ""){
    //   //window.alert('Fill all fields.');
    //   this.alertService.error('Fill all fields');
    //   setTimeout(function(){
    //     window.location.reload();
    //   },500);
    // }
    else {
      //window.alert('Error! incorrect email or password.');
      this.alertService.error('Invalid Username or Password');
      setTimeout(function () {
        window.location.reload();
      }, 500);
    }
  }
}


