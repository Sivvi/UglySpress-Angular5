import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AlertServiceService } from '././_services/alert-service/alert-service.service';
import { HttpService } from '././_services/http/http.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header/header.component';
import { FooterComponent } from './footer/footer/footer.component';
import { LoginComponent } from './login/login/login.component';
import { ListComponent } from './list/list/list.component';
import { AlertComponent } from './shared/alert/alert.component';

import { Routes, RouterModule, Router } from '@angular/router';
import { ListHeaderComponent } from './shared/list-header/list-header.component';

import { Storenames } from './store.pipe';

const appRoutes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'list',
    component: ListComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    ListComponent,
    AlertComponent,
    ListHeaderComponent,
    Storenames
  ],
  imports: [
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    AlertServiceService,
    HttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor() { }
}
