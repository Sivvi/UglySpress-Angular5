import { Pipe, PipeTransform } from '@angular/core';


@Pipe({name: 'storenames'})
export class Storenames  {
   
   // public value = Array();
    transform(value){ 
       if (value == '1') {return 'UAE English';}
       if (value == '2') {return 'UAE Arabic';}
       if (value == '3') {return 'KSA English';}
       if (value == '4') {return 'KSA Arabic';}
       if (value == '5') {return 'Oman English';}
       if (value == '6') {return 'Oman Arabic';}
       if (value == '7') {return 'Bahrain English';}
       if (value == '8') {return 'Bahrain Arabic';}
       if (value == '9') {return 'Kuwait English';}
       if (value == '10') {return 'Kuwait Arabic';}
       if (value == '11') {return 'Qatar English';}
       if (value == '12') {return 'Qatar Arabic';}
       if (value == '13') {return 'Intl English';}
       if (value == '14') {return 'Intl Arabic';}
     }
  }
