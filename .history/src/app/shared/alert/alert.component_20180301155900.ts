 import { Component, OnInit } from '@angular/core';
 import { AlertServiceService } from '../../_services/alert-service.service';
 import { timeout } from 'rxjs/operator/timeout';
 @Component({
    selector: 'alert',
    templateUrl: 'alert.component.html',
 })

 export class AlertComponent {
   message: any;

   constructor(private alertService: AlertServiceService) { }

   ngOnInit() {
       this.alertService.getMessage().subscribe(message => {        
       this.message = message;
       this.alertService.getMessage().timeout(200);
       console.log(message);
      });
      
}
 }