import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AlertServiceService } from '././_services/alert-service.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header/header.component';
import { FooterComponent } from './footer/footer/footer.component';
import { LoginComponent } from './login/login/login.component';
import { ListComponent } from './list/list/list.component';
import { Routes, RouterModule, Router } from '@angular/router';
import { AlertComponent } from './shared/alert/alert.component';

const appRoutes:Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'list',
    component: ListComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    ListComponent,
    AlertComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AppRoutingModule,
    AlertComponent.forRoot({timeout: 500})
  ],
  providers: [AlertServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
