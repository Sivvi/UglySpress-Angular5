 import { Component, OnInit } from '@angular/core';
 import { AlertService } from '../Services/alert_services';

 @Component({
    selector: 'alert',
    templateUrl: 'alert.html'
 })

 export class AlertComponent {
   message: any;

   constructor(private alertService: AlertService) { }

   ngOnInit() {
      this.alertService.getMessage().subscribe(message => { this.message = message; });
   }
 }