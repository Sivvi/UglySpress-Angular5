 import { Component, OnInit } from '@angular/core';
 import { AlertServiceService } from '../_services/alert-service.service';

 @Component({
    selector: 'alert',
    templateUrl: 'alert/alert.html',
    providers: [AlertServiceService],
 })

 export class AlertComponent {
   message: any;

   constructor(private alertService: AlertServiceService) { }

   ngOnInit() {
    this.alertService.getMessage().subscribe(message => {         this.message = message; });
}
 }