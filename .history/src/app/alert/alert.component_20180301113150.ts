 import { Component, OnInit } from '@angular/core';
 import { AlertService } from '../_services/alert-service.service';

 @Component({
    selector: 'alert',
    templateUrl: 'alert.html'
 })

 export class AlertComponent {
   message: any;

   constructor(private alertService: AlertService) { }

   ngOnInit() {
      this.alertService.getMessage().subscribe(message => { this.message = message; });
   }
 }