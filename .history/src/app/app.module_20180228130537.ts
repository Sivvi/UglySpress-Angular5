import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header/header.component';
import { FooterComponent } from './footer/footer/footer.component';
import { LoginComponent } from './login/login/login.component';
import { ListComponent } from './list/list/list.component';
import { RouterModule, Routes} from '@angular/router'

const appRoutes:AppRoutingModule = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'list',
    component: ListComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
