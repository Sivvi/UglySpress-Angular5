
import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AlertServiceService implements OnInit {
    public message = false;
    private subject = new Subject();
    private keepAfterNavigationChange = false;
    ngOnInit() {
        let self = this;
        this.message = true;
    }  
    

    constructor(private router: Router) {
        
       // clear alert message on route change
       router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    this.keepAfterNavigationChange = false;
                    
                } else {
                    // clear alert
                    this.subject.next();
                    this.message = true;   
                    console.log('2test');
                }
               
            }
            setTimeout(function(){
                console.log('happeeee');
                self.message = false;  
              },300);
        });
   }
   

   success(message: string, keepAfterNavigationChange = false) {
       this.keepAfterNavigationChange = keepAfterNavigationChange;
       this.subject.next({ type: 'success', text: message });
   }
   error(message: string, keepAfterNavigationChange = false) {
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.subject.next({ type: 'error', text: message });
    
}

    


getMessage() {
    return this.subject.asObservable();
}
}

