
import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AlertServiceService {
    private subject = new Subject();
    private keepAfterNavigationChange = false;

    public active = false;

    constructor(private router: Router) {
        
       // clear alert message on route change
       router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    this.keepAfterNavigationChange = false;
                    this.active = true;  
                } else {
                    // clear alert
                    this.subject.next();
                    this.active = false;  
                }
                setTimeout(function(){
                    self.active = false;
                  },3000);
            }
        });
   }
   public close(){
    this.active = false;
  }

   success(message: string, keepAfterNavigationChange = false) {
       this.keepAfterNavigationChange = keepAfterNavigationChange;
       this.subject.next({ type: 'success', text: message });
   }
   error(message: string, keepAfterNavigationChange = false) {
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.subject.next({ type: 'error', text: message });
    
}

    


getMessage() {
    return this.subject.asObservable();
}
}

