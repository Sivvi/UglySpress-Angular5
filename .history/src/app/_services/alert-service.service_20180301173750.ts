
import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AlertServiceService implements OnInit {
    private subject = new Subject();
    private keepAfterNavigationChange = false;

    public active = false;

  


ngOnInit() {
    constructor(private router: Router) {
        
        // clear alert message on route change
        router.events.subscribe(event => {
             if (event instanceof NavigationStart) {
                 if (this.keepAfterNavigationChange) {
                     // only keep for a single location change
                     this.keepAfterNavigationChange = false;
                 } else {
                     // clear alert
                     this.subject.next();
                 }
             }
         });
    }
    success(message: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message });
    }
    error(message: string, keepAfterNavigationChange = false) {
     this.keepAfterNavigationChange = keepAfterNavigationChange;
     this.subject.next({ type: 'error', text: message });
     this.active = true;  
 }
    setTimeout(function(){
        self.active = false;
      },3000);
}

getMessage() {
    return this.subject.asObservable();
}
}

